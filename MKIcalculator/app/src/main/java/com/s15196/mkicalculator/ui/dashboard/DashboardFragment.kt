package com.s15196.mkicalculator.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat.getDrawable
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.s15196.mkicalculator.R
import com.s15196.mkicalculator.afterTextChanged

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel = ViewModelProviders.of(this, DashboardViewModelFactory()).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)

        val mass: EditText = root.findViewById(R.id.massEditText)
        val height: EditText = root.findViewById(R.id.heightEditText)
        val age: EditText = root.findViewById(R.id.ageEditText)
        val sex: Switch = root.findViewById(R.id.my_sex)
        val bmi: TextView = root.findViewById(R.id.totalTextView)
        val status: TextView = root.findViewById(R.id.bmiStatusTextView)
        val calories: TextView = root.findViewById(R.id.calories)
        val meal: TextView = root.findViewById(R.id.sample_meal)
        val recipe: TextView = root.findViewById(R.id.recipy)
        val image: ImageView = root.findViewById(R.id.recipy_image)

        mass.afterTextChanged {
            calculateBMI(mass, height)
            calculateCalories(mass, height, age, sex)
        }
        height.afterTextChanged {
            calculateBMI(mass, height)
            calculateCalories(mass, height, age, sex)
        }
        age.afterTextChanged {
            calculateCalories(mass, height, age, sex)
        }
        sex.setOnCheckedChangeListener{ _, _ ->
            calculateCalories(mass, height, age, sex)
        }

        dashboardViewModel.BMI.observe(this, Observer {
            bmi.text = it.toString()
        })
        dashboardViewModel.Result.observe(this, Observer {
            status.setText(it)
            when (it) {
                R.string.underweight -> image.setImageDrawable(getDrawable(resources,R.drawable.underweight, null))
                R.string.normalweight -> image.setImageDrawable(getDrawable(resources,R.drawable.normalweight, null))
                R.string.overweight -> image.setImageDrawable(getDrawable(resources,R.drawable.overweight, null))
                R.string.obese -> image.setImageDrawable(getDrawable(resources,R.drawable.obese, null))
                else -> image.setImageDrawable(null)
            }
        })
        dashboardViewModel.Calories.observe(this, Observer {
            calories.text = it.toString()
        })
        dashboardViewModel.Meal.observe(this, Observer {
            meal.setText(it)
        })
        dashboardViewModel.Recipe.observe(this, Observer {
            recipe.setText(it)
        })
        return root
    }

    private fun calculateBMI(mass: EditText, height: EditText){
        if(mass.text.isNotEmpty() && height.text.isNotEmpty()){
            dashboardViewModel.calculateBMI(
                mass.text.toString().toDouble(),
                height.text.toString().toDouble()
            )
        }else{
            dashboardViewModel.calculateBMI(0.0,0.0)
        }
    }

    private fun calculateCalories(mass: EditText, height: EditText, age: EditText, sex: Switch){
        if(height.text.isNotEmpty() && mass.text.isNotEmpty() && age.text.isNotEmpty())
        {
            when {
                //woman
                sex.isChecked && age.text.toString().toInt() > 0-> {
                    dashboardViewModel.calculateCalories(
                        mass.text.toString().toDouble(),
                        height.text.toString().toDouble(),
                        age.text.toString().toInt(),
                        true
                    )
                }
                //female
                else -> {
                    dashboardViewModel.calculateCalories(
                        mass.text.toString().toDouble(),
                        height.text.toString().toDouble(),
                        age.text.toString().toInt(),
                        false
                    )
                }
            }
        } else {
            dashboardViewModel.calculateCalories(0.0,0.0,0,true)
        }
    }
}
