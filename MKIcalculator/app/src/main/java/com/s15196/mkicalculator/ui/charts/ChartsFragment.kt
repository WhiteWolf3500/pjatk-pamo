package com.s15196.mkicalculator.ui.charts

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.s15196.mkicalculator.R

class ChartsFragment : Fragment() {

    private lateinit var chartsViewModel: ChartsViewModel

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        chartsViewModel =
            ViewModelProviders.of(this).get(ChartsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_charts, container, false)
        val webView: WebView = root.findViewById(R.id.webview)
        webView.settings.javaScriptEnabled = true
        chartsViewModel.text.observe(this, Observer {
            webView.loadData(it,"text/html","utf-8")
        })
        return root
    }
}