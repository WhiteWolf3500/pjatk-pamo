package com.s15196.mkicalculator.data

class DashboardRepository(val dataSource: DashboardDataSource) {

    init {
    }

    fun calculateBMI(mass: Double, height: Double): Double {
        return dataSource.calculateBMI(mass, height)
    }

    fun calculateCalories(mass: Double, height: Double, age: Int, sex: Boolean): Double{
        return dataSource.calculateCalories(mass, height, age, sex)
    }
}