package com.s15196.mkicalculator.data

import kotlin.math.pow
import kotlin.math.round

class DashboardDataSource {

    fun calculateBMI(mass: Double, height: Double): Double {
        return round(mass*100000 / height.pow(2.0))/10
    }

    fun calculateCalories(mass: Double, height: Double, age: Int, sex: Boolean): Double {
        return if (sex){
            // woman
            ((10*mass)+(6.25*height)-(5*age)-161)
        }else{
            // man
            ((10*mass)+(6.25 * height)-(5*age)+5)
        }
    }
}

