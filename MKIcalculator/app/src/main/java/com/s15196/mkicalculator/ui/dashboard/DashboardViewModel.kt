package com.s15196.mkicalculator.ui.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.s15196.mkicalculator.R
import com.s15196.mkicalculator.data.DashboardRepository

class DashboardViewModel(private val dashboardRepository: DashboardRepository) : ViewModel() {

    private var _bmi = MutableLiveData<Double>()
    val BMI = _bmi

    private val _result = MutableLiveData<Int>()
    val Result = _result

    private var _calories = MutableLiveData<Double>()
    val Calories = _calories

    private var _meal = MutableLiveData<Int>()
    val Meal = _meal

    private var _recipe = MutableLiveData<Int>()
    val Recipe = _recipe

    fun calculateBMI(mass: Double, height: Double){
        val bmi = dashboardRepository.calculateBMI(mass, height)

        if(bmi > 0){
            _bmi.value = bmi
            if (bmi < 18.5) {
                _result.value = R.string.underweight
                _meal.value = R.string.underweight_meal
                _recipe.value = R.string.underweight_recipy
            }
            else if (bmi < 25.0) {
                _result.value = R.string.normalweight
                _meal.value = R.string.normalweight_meal
                _recipe.value = R.string.normalweight_recipy
            }
            else if (bmi < 30.0){
                _result.value = R.string.overweight
                _meal.value = R.string.overweight_meal
                _recipe.value = R.string.overweight_recipy
            }
            else{
                _result.value = R.string.obese
                _meal.value = R.string.obese_meal
                _recipe.value = R.string.obese_recipy
            }
        }else{
            _bmi.value = 0.0
            _result.value = R.string.empty
            _meal.value = R.string.empty
            _recipe.value = R.string.empty
        }
    }

    fun calculateCalories(mass: Double, height: Double, age: Int, sex: Boolean){
        val calories = dashboardRepository.calculateCalories(mass, height, age, sex)

        if (calories > 0){
            _calories.value = calories
        }else{
            _calories.value = 0.0
        }
    }
}