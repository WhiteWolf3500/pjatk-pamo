package com.s15196.mkicalculator.ui.charts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ChartsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "<!DOCTYPE html>\n" +
                "<html lang=\"pl-PL\">\n" +
                "<body>\n" +

                "<div id=\"chart\"></div>\n" +

                "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "<script type=\"text/javascript\">\n" +
                "google.charts.load('current', {packages: ['corechart', 'bar']});\n" +
                "google.charts.setOnLoadCallback(drawBasic);\n" +

                "function drawBasic() {\n" +
                "      var data = google.visualization.arrayToDataTable([\n" +
                "        ['Miesiac', 'Waga',],\n" +
                "        ['Styczen', 80],\n" +
                "        ['Luty', 75],\n" +
                "        ['Marzec', 78],\n" +
                "        ['Kwieciej', 81],\n" +
                "        ['Maj', 76],\n" +
                "        ['Czerwiec', 74]\n" +
                "      ]);\n" +
                "      \n" +
                "      var options = {\n" +
                "        title: 'Masa ciala w czasie',\n" +
                "        hAxis: {\n" +
                "          minValue: 0\n" +
                "        },\n" +
                "        vAxis: {\n" +
                "          minValue: 0\n" +
                "        }\n" +
                "      };\n" +
                "      \n" +
                "      var chart = new google.visualization.ColumnChart(\n" +
                "        document.getElementById('chart'));\n" +

                "      chart.draw(data, options);\n" +
                "    }\n" +
                "</script>\n" +

                "</body>\n" +
                "</html>\n";
    }
    val text: LiveData<String> = _text
}