1. 10.10.2019
Zaimplementuj kalkulator BMI.

2. 11.10.2019
Rozszerz poprzednią aplikację:
   - użyj Bottom Navigation Acitivity
      (na pierwszym przycisku - ekran powitalny; 
	na drugim BMI kalkulator, 
	na trzecim rekomendacja kalorii)
   - zarekomenduj użytkownikowi ilość kalorii za pomocą wzoru Mifflina
   - zaproponuj użytkownik przepis kulinarny; dodaj obrazek gotowego dania
   - dodaj 4 obrazki z przykładowymi przepisami;

3. 24.10.2019
Rozszerz poprzednią aplikację:
    - dodaj quiz, dotyczący np. zdrowego trybu życia; min. 5 pytań;
    - dodaj wykres, np. masy ciała w czasie;

4. 21.11.2019
Zaimplementuj funkcjonalności do tej pory stworzoną aplikację w języku Kotlin.

Termin: 30.11.2019
