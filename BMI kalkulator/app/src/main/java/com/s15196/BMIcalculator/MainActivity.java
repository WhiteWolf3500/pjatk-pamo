package com.s15196.BMIcalculator;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private double height = 0;
    private double mass = 0;
    private double age = 0;


    private TextView totalTextView;
    private TextView bmiStatusTextView;
    private TextView caloriesStatusTextView;
    private TextView sampleMeal;
    private TextView mealRecipy;
    private ImageView mealImage;
    private android.support.v7.widget.GridLayout Home;
    private android.support.v7.widget.GridLayout BMIcalculator;
    private android.support.v7.widget.GridLayout Recomendations;
    private android.support.v7.widget.GridLayout Charts;
    private android.support.v7.widget.GridLayout Quiz;
    private Switch sex;

    // called when the activity is first created
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // call superclass onCreate
        setContentView(R.layout.activity_main); // inflate the GUI
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // get references to programmatically manipulated TextViews
        bmiStatusTextView = findViewById(R.id.bmiStatusTextView);
        caloriesStatusTextView = findViewById(R.id.calories);
        totalTextView = findViewById(R.id.totalTextView);
        sampleMeal = findViewById(R.id.sample_meal);
        mealImage = findViewById(R.id.recipy_image);
        mealRecipy = findViewById(R.id.recipy);
        Home = findViewById(R.id.Home);
        BMIcalculator = findViewById(R.id.BMIcalculator);
        Recomendations = findViewById(R.id.Recomendations);
        Charts = findViewById(R.id.Charts);
        Quiz = findViewById(R.id.Quiz);

        // set home visible as first page
        Home.setVisibility(View.VISIBLE);

        // set heightEditText's TextWatcher
        EditText heightEditText = findViewById(R.id.heightEditText);
        heightEditText.addTextChangedListener(HeightEditTextWatcher);

        // set massEditText's TextWatcher
        EditText massEditText = findViewById(R.id.massEditText);
        massEditText.addTextChangedListener(MassEditTextWatcher);

        // set ageEditText's TextWatcher
        EditText ageEditText = findViewById(R.id.ageEditText);
        ageEditText.addTextChangedListener(AgeEditTextWatcher);

        // set sexSwitch's ChangeWatcher
        sex = findViewById(R.id.my_sex);
        sex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (age > 0) calculate();
            }
        });

        // setup the WebView content
        WebView chartWebview = findViewById(R.id.chart_webview);
        WebSettings webSettings = chartWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        chartWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        String htmlData = "<!DOCTYPE html>\n" +
                "<html lang=\"pl-PL\">\n" +
                "<body>\n" +

                "<div id=\"chart\"></div>\n" +

                "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>\n" +
                "<script type=\"text/javascript\">\n" +
                "google.charts.load('current', {packages: ['corechart', 'bar']});\n" +
                "google.charts.setOnLoadCallback(drawBasic);\n" +

                "function drawBasic() {\n" +
                "      var data = google.visualization.arrayToDataTable([\n" +
                "        ['Miesiac', 'Waga',],\n" +
                "        ['Styczen', 80],\n" +
                "        ['Luty', 75],\n" +
                "        ['Marzec', 78],\n" +
                "        ['Kwieciej', 81],\n" +
                "        ['Maj', 76],\n" +
                "        ['Czerwiec', 74]\n" +
                "      ]);\n" +
                "      \n" +
                "      var options = {\n" +
                "        title: 'Masa ciala w czasie',\n" +
                "        hAxis: {\n" +
                "          minValue: 0\n" +
                "        },\n" +
                "        vAxis: {\n" +
                "          minValue: 0\n" +
                "        }\n" +
                "      };\n" +
                "      \n" +
                "      var chart = new google.visualization.ColumnChart(\n" +
                "        document.getElementById('chart'));\n" +

                "      chart.draw(data, options);\n" +
                "    }\n" +
                "</script>\n" +

                "</body>\n" +
                "</html>\n";
        chartWebview.loadData(htmlData, "text/html", "UTF-8");
    }

    // calculate and display tip and total amounts
    private void calculate() {

        // calculate the bmi total
        double bmi = 0;
        if (height > 0 && mass > 0) {
            bmi = Math.round((mass / (height * height))*100);
            bmi /= 100;

            if (bmi < 18.5) {
                bmiStatusTextView.setText(getString(R.string.underweight));
                sampleMeal.setText(getString(R.string.underweight_meal));
                mealImage.setImageDrawable(getResources().getDrawable(R.drawable.underweight));
                mealRecipy.setText(getString(R.string.underweight_recipy));
            }
            else if (bmi > 30) {
                bmiStatusTextView.setText(getString(R.string.obese));
                sampleMeal.setText(getString(R.string.obese_meal));
                mealImage.setImageDrawable(getResources().getDrawable(R.drawable.obese));
                mealRecipy.setText(getString(R.string.obese_recipy));
            }
            else if (bmi > 25) {
                bmiStatusTextView.setText(getString(R.string.overweight));
                sampleMeal.setText(getString(R.string.overweight_meal));
                mealImage.setImageDrawable(getResources().getDrawable(R.drawable.overweight));
                mealRecipy.setText(getString(R.string.overweight_meal));
            }
            else {
                bmiStatusTextView.setText(getString(R.string.normalweight));
                sampleMeal.setText(getString(R.string.normalweight_meal));
                mealImage.setImageDrawable(getResources().getDrawable(R.drawable.normalweight));
                mealRecipy.setText(getString(R.string.normalweight_meal));
            }

            double calories;
            if (sex.isChecked() && age > 0) {
                // woman
                calories = (10*mass)+(6.25*height*100)-(5*age)-161;
                caloriesStatusTextView.setText(String.valueOf(calories));
            }else if (age > 0){
                // man
                calories = (10*mass)+(6.25*height*100)-(5*age)+5;
                caloriesStatusTextView.setText(String.valueOf(calories));
            }else caloriesStatusTextView.setText(getString(R.string.caloriesError1));
        }else{
            bmiStatusTextView.setText("");
            sampleMeal.setText("");
            mealRecipy.setText("");
            caloriesStatusTextView.setText(getString(R.string.caloriesError2));
            mealImage.setImageDrawable(null);
        }

        // display tip and total formatted as currency
        totalTextView.setText(String.valueOf(bmi));
    }

    // listener object for the MassEditText's text-changed events
    private final TextWatcher MassEditTextWatcher = new TextWatcher() {
        // called when the user modifies the bill amount
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try { // get bill amount and display currency formatted value
                mass = Double.parseDouble(s.toString());
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                mass = 0.0;
            }
            calculate(); // update the tip and total TextViews
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    };

    // listener object for the HeightEditText's text-changed events
    private final TextWatcher HeightEditTextWatcher = new TextWatcher() {
        // called when the user modifies the bill amount
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try { // get bill amount and display currency formatted value
                height = Double.parseDouble(s.toString())/100;
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                height = 0.0;
            }
            calculate(); // update the tip and total TextViews
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    };

    // listener object for the AgeEditText's text-changed events
    private final TextWatcher AgeEditTextWatcher = new TextWatcher() {
        // called when the user modifies the bill amount
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            try { // get bill amount and display currency formatted value
                age = Double.parseDouble(s.toString());
            }
            catch (NumberFormatException e) { // if s is empty or non-numeric
                age = 0.0;
            }
            calculate(); // update the tip and total TextViews
        }

        @Override
        public void afterTextChanged(Editable s) { }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    };

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Home.setVisibility(View.GONE);
            BMIcalculator.setVisibility(View.GONE);
            Recomendations.setVisibility(View.GONE);
            Charts.setVisibility(View.GONE);
            Quiz.setVisibility(View.GONE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Home.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_dashboard:
                    BMIcalculator.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_notifications:
                    Recomendations.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_charts:
                    Charts.setVisibility(View.VISIBLE);
                    return true;
                case R.id.navigation_quiz:
                    Quiz.setVisibility(View.VISIBLE);
                    launchActivity();
                    return true;
            }
            return false;
        }
    };

    private void launchActivity() {

        Intent intent = new Intent(MainActivity.this, QuizActivity.class);
        startActivity(intent);
    }
}
